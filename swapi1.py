#!usr/bin/python3

"Star Wars API HTTP response parsing"

from pprint import pprint
import requests

URL= "https://swapi.dev/api/people/four"

def main():

    resp = requests.get(URL)
    vader = resp.json()

    pprint(vader)

if __name__ == '__main__':
    main()
