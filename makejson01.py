#!usr/bin/python3

"Parsing JSON files in Python"

import json

def main():
    
    hitchhikers = [{"name": "Zaphod Beeblebrox", "species": "Betelgeusian"},{"name": "Arthur Dent", "species": "Human"}]

    print(hitchhikers)

    with open("galaxyguide.json", "w") as temp:

        json.dump(hitchhikers, temp)

if __name__ == "__main__":

    main()
