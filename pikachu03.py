#!/usr/bin/python3

import requests
import pandas as pd


ITEMURL = "http://pokeapi.co/api/v2/item/"

def main():

    items = requests.get(f"{ITEMURL}?limit=1000")
    items = items.json()

    search_key = input("Enter word to search in Pokemon API. Default word is 'ball' if nothing is provided : ") or 'ball'
    searchwords = []

    for item in items.get("results"):
        if search_key in item.get("name"):
            searchwords.append(item.get("name"))

    print("There are {} words that contain the word 'heal' in the Pokemon Item API!".format(len(searchwords)))

    print("List of Pokemon items containing heal: ")
    print(searchwords)

    searchwords_df = pd.DataFrame(searchwords, columns = ['Search_Words'])
    #searchwords_df.to_excel("pokemonitems.xlsx", index=False)
    searchwords_df.to_excel("pokemon/pokemonitems.xlsx", index=False)

if __name__ == "__main__":
    main()
