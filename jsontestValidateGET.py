#!usr/bin/python3

import requests
import json

IPURL = "http://ip.jsontest.com/"

def main():
    
    #test data to validate JSON

    mydata = {"fruit": ["apple", "pear"], "vegetable": ["carrot"]}
    jsonToValidate = f"json={ json.dumps(mydata).replace(' ', '') }"
    
    GETURL = "http://validate.jsontest.com/"

    resp = requests.get(f"{GETURL}?{jsonToValidate}")
    resp_json = resp.json()

    print("Is your response valid ? {}".format(resp_json.get('validate')))

if __name__ == "__main__":
    main()
