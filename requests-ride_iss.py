#!/usr/bin/python3
"""requests library"""

# notice we no longer need to import urllib.request or json
import requests

## Define URL
MAJORTOM = 'http://api.open-notify.org/astros.json'

def main():

    groundctrl = requests.get(MAJORTOM)
    helmetson = groundctrl.json()
    print(helmetson)

    print('\n\nPeople in Space: ', helmetson['number'])
    people = helmetson['people']
    print(people)

if __name__ == "__main__":
    main()
