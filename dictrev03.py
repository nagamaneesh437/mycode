#!/usr/bin/python3

def main():
    """run time code"""
    vendordict = {'cisco': True, 'juniper': False, 'arista': True, 'netgear': True}
    custlist = ['acme', 'globex corporation', 'soylent green', 'initech', 'umbrella']

    print(vendordict)
    print(vendordict.keys())
    print(vendordict.values())
    print(vendordict.get('juniper'))
    vendordict.pop('netgear')
    print(vendordict.get('netgear'))

    custlist.append('cyberdyne')
    print(custlist)

if __name__ == '__main__':
    main()
