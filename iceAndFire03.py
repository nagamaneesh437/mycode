#!/usr/bin/python3
"Exploring OpenAPIs with requests"

import requests

AOIF_BOOKS = "https://www.anapioficeandfire.com/api/books"

def main():

    gotresp = requests.get(AOIF_BOOKS)
    got_dj = gotresp.json()

    ## loop across response
    for singlebook in got_dj:
        print(f"{singlebook.get('name')}, pages - {singlebook.get('numberOfPages')}")
        print(f"\tAPI URL -> {singlebook.get('url')}\n")
        print(f"\tISBN -> {singlebook.get('isbn')}\n")
        print(f"\tPUBLISHER -> {singlebook.get('publisher')}\n")
        print(f"\tNo. of CHARACTERS -> {len(singlebook.get('characters'))}\n")

if __name__ == "__main__":
    main()
