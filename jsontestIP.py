#!usr/bin/python3

import requests

IPURL = "http://ip.jsontest.com/"

def main():
    
    resp = requests.get(IPURL)
    resp_json = resp.json()
    
    print(resp_json)
    print("The current wap IP is {}".format(resp_json.get('ip')))

if __name__ == "__main__":
    main()
