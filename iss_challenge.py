#!/usr/bin/python3
"exploring urllib"""

import urllib.request as req
import json

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    groundctrl = req.urlopen(MAJORTOM)

    helmet = groundctrl.read()
    helmetson = json.loads(helmet.decode("utf-8"))
    
    print("People in space: {}".format(helmetson.get("number")))

    for astro in helmetson["people"]:
        print("{} on the {}".format(astro.get("name"), astro.get("craft")))

if __name__ == "__main__":
    main()
