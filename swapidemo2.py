from pprint import pprint
import requests

URL= "https://swapi.dev/api/people/1"

def main():

    resp = requests.get(URL)
    print(resp.status_code)
    print(resp.reason)

    pprint(dict(resp.headers))

if __name__ == "__main__":
    main()
