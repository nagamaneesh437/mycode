#!usr/bin/python3

import requests
from pprint import pprint

URL = "https://swapi.dev/api/people/4/"
URL2 = "https://swapi.dev/api/films/1/"
URL3 = "https://swapi.dev/api/starships/13/"

def main():

    resp = requests.get(URL)
    resp2 = requests.get(URL2)
    resp3 = requests.get(URL3)

    if ((resp.status_code == 200) & (resp2.status_code == 200) & (resp3.status_code == 200)):

        vader = resp.json()
        pprint(vader)

        vader2 = resp2.json()
        pprint(vader2)

        vader3 = resp3.json()
        pprint(vader3)
        
        print("Customization 1 :")
        print("{} was born in the year {}. His eyes are now {} and his hair color is {}".format(vader['name'], vader['birth_year'], vader['eye_color'], vader['hair_color']))

        print("Customization 2:")
        print("He first appeared in the movie {} and could be found flying around in his {}.".format(vader2['title'], vader3['name']))

    else:

        pprint("This is not valid URL")

if __name__ == "__main__":
    main()
