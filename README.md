# mycode (Project Title)

This project contains different codes on building APIs using Python

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Python](https://www.python.org/) - The coding language used

## Authors

* Naga Maneesh K - *Initial work*
