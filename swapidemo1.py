#!usr/bin/env python3

import requests
URL = "https://swapi.dev/api/people/1"

def main():

    resp = requests.get(URL)
    print("This object class is:", type(resp), "\n")
    print("Methods/Attributes included:", dir(resp))

if __name__ == "__main__":
    main()
