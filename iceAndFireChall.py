import requests
import pprint

AOIF_CHAR = "https://www.anapioficeandfire.com/api/characters/"

def main():

    got_charToLookup = input("Pick a number between 1 and 1000 to return info on a GoT character - ")
    gotresp = requests.get(AOIF_CHAR + got_charToLookup)
    got_dj = gotresp.json()

    houses_api_list = got_dj.get('allegiances')
    books_api_list = got_dj.get('books')

    houses = []
    for house_api in houses_api_list:

        house_resp = requests.get(house_api)
        house_resp_json = house_resp.json()

        houses.append(house_resp_json.get('name'))
    print("\nThis character belongs to the following houses:")
    print(houses)

    books = []
    for books_api in books_api_list:

        book_resp = requests.get(books_api)
        book_resp_json = book_resp.json()

        books.append(book_resp_json.get('name'))

    print("\nThis character appears in the following books:")
    print(books)

if __name__ == "__main__":
        main()
