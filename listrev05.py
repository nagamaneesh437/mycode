#!/usr/bin/python3
"Learning or Reviewing about Lists"

def main():
    alta3classes = ['python_basics', 'python_api_design', 'python_for_networking', 'kubernetes', \
      'sip', 'ims', '5g', '4g', 'avaya', 'ansible', 'python_and_ansible_for_network_automation']

    #display the list
    print(alta3classes)
	
	#size of list
    print(len(alta3classes))

    # accessing elements based on index
    print(alta3classes[0])

    # display SIP
    print(alta3classes[4])

    # display Ansible
    print(alta3classes[9])

    ##Uncomment to see a list index out of range error
    #print(alta3classes[99])
	
	#list slicing
    print(alta3classes[0:3])

    print(alta3classes[2:5])

    print(alta3classes[-1])

if __name__ == "__main__":
    main()
