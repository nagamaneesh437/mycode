#!/usr/bin/python3

import requests

# define the URL we want to use
POSTURL = "http://validate.jsontest.com/"

def main():
    mydata = {"json": "{'fruit': ['apple', 'pear'], 'vegetable': ['carrot']}" }
    resp = requests.post(POSTURL, data=mydata)
    respjson = resp.json()
    print(respjson)

    print("Is your JSON Valid? {}".format(respjson.get('validate')))

if __name__ == "__main__":
    main()
