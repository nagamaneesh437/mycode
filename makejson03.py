#!usr/bin/python3

"Accessing json from a file"

import json

def main():

    with open("datacenter.json", "r") as datacenter:

        datacenterstring = datacenter.read()

    print(datacenterstring)
    print(type(datacenterstring))

    datacenterdecoded = json.loads(datacenterstring)

    print(type(datacenterdecoded))

    print(datacenterdecoded)

    print(datacenterdecoded["row3"])

    print(datacenterdecoded["row2"][1])

if __name__ == "__main__":

    main()
